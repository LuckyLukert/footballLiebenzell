#!/usr/bin/env python3.6
# coding=utf-8

import logging

from entity import Entity
from geometry import *
from settings import *


def connectedEvent(event, game): logging.debug("connectedEvent: "+str(event.sender))
def playerJoinedEvent(event, game): logging.debug("playerJoinedEvent: "+str(event.sender))
def playerLeftEvent(event, game): logging.debug("playerLeftEvent: "+str(event.sender))
def gameStateEvent(event, game): logging.debug("gameStateEvent: "+str(event.sender))
def goalEvent(event, game): logging.debug("goalEvent: "+str(event.sender))


def disconnectedEvent(event, game):
    logging.debug("disconnectedEvent: "+str(event.sender))
    if game.removePlayer(event.sender):
        game.server.broadcast(Event({"event":"playerLeft", "id": event.sender}))
    
def connectObserverEvent(event, game):
    logging.debug("connectObserverEvent: "+str(event.sender))
    
def connectEvent(event, game):
    logging.debug("connectEvent: "+str(event.sender))
    if not hasattr(event, "name"):
        event.__dict__["name"] = "Peter"
    player = Entity(Point(0,0), PLAYER_SIZE, PLAYER_FRICTION, event.name)
    
    conDict = {"event": "connected"}
    conDict["id"] = event.sender
    conDict["width"] = WIDTH
    conDict["height"] = HEIGHT
    conDict["playerSize"] = PLAYER_SIZE
    conDict["ballSize"] = BALL_SIZE
    conDict["goalSize"] = GOAL_SIZE
    conDict["goalDepth"] = GOAL_DEPTH
    conDict["team"] = game.addPlayer(event.sender, player)
    game.server.send(event.sender, Event(conDict))
    
    for (id, player) in game.players.items():  #send each playerJoin to himself only
        if id == conDict["id"]:
            continue
        individualJoinDict = {"event": "playerJoined"}
        individualJoinDict["id"] = id
        individualJoinDict["name"] = player.name
        if id in game.teams[0]:
            individualJoinDict["team"] = 0
        else:
            individualJoinDict["team"] = 1
        game.server.send(event.sender, Event(individualJoinDict))
        
    
    joinDict = {"event": "playerJoined"}
    joinDict["id"] = event.sender
    joinDict["name"] = event.name
    joinDict["team"] = conDict["team"]
    game.server.broadcast(Event(joinDict))

def kickEvent(event, game):
    logging.debug("kickEvent: "+str(event.sender))
    game.players[event.sender].kicking = True
    
def accelEvent(event, game):
    logging.debug("accelEvent: "+str(event.sender))
    if not hasattr(event, "ax") or not hasattr(event, "ay"):
        logging.error("no acceleration given")
        return
    game.players[event.sender].changeAccel(Vector(event.ax, event.ay))




eventsExecute = {
    "connected": connectedEvent,
    "playerJoined": playerJoinedEvent,
    "playerLeft": playerLeftEvent,
    "gameState": gameStateEvent,
    "goal": goalEvent,
    "disconnected": disconnectedEvent,
    "connectObserver": connectObserverEvent,
    "connect": connectEvent,
    "kick": kickEvent,
    "accel": accelEvent,
    'test': lambda event, game: logging.debug("Test Event from "+str(event.sender)+": " + event.message)
}




class Event:
    def __init__(self, entries, sender=None):
        self.event = None
        self.sender = sender
        self.__dict__.update(entries)

    def execute(self, game):
        try:
            with game.lock:
                eventsExecute[self.event](self, game)
        except KeyError:
            pass

    def reprJSON(self):
        enc = self.__dict__.copy()
        enc.pop("sender")
        return enc
