#!/usr/bin/env python3.6
# coding=utf-8
import logging
import socket
import threading
from tkinter import *

import time

from client import HumanClient
from settings import *

from game import Game
from network import ServerThread



class GameCanvas(Canvas):
    def __init__(self, parent, game :Game, **kwargs):
        Canvas.__init__(self, parent, **kwargs)
        self.bind("<Configure>", self.on_resize)
        self.height = self.winfo_reqheight()
        self.width = self.winfo_reqwidth()
        self.game :Game = game

    def on_resize(self,event):
        self.width = event.width
        self.height = event.height
        self.config(width=self.width, height=self.height)
        self.game.redraw()




class Gui:
    def __init__(self, game, keyListenerList :list):
        self.top = Tk()
        self.canvas = GameCanvas(self.top, game, bg="green")
        self.keyListenerList = keyListenerList
        self.top.bind('<KeyPress>', self.onKeyPress)
        self.top.bind("<KeyRelease>", self.onKeyUp)
    
    def onKeyPress(self, event):
        for keyListener in self.keyListenerList:
            keyListener.onKeyPress(event)
    def onKeyUp(self, event):
        for keyListener in self.keyListenerList:
            keyListener.onKeyUp(event)
        
    
    def start(self):
        self.canvas.pack(fill=BOTH, expand=YES)
        self.canvas.update()
        self.step()
        self.top.mainloop()
    
    def step(self):
        self.canvas.after(STEP_TIMING, self.step)
        self.canvas.game.step()
        self.canvas.game.reallyRedraw(self.canvas)




    
    
    
    
    
    
    
    
    
    