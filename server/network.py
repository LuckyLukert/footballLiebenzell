#!/usr/bin/env python3.6
# coding=utf-8
import json
import logging
import socket
import threading

import sys

from json import JSONEncoder
from queue import Queue

from game import Game
from settings import *
from event import Event
import time


class FakeServer:
    def __init__(self):
        pass
    def start(self, game):
        logging.debug("Fake server started")
    def send(self, id, event):
        logging.debug("FakeMessage to "+ str(id))
    def broadcast(self, event):
        logging.debug("FakeBroadcast")



class ServerThread:

    listening = True

    def __init__(self):
        self.lock = threading.RLock()  #lock for clients and currentId
        self.clients = dict()
        self.currentId = 0
        self.sendQueue :Queue = Queue()
    

    def start(self, game, ip, port):
        self.ip = ip
        self.port = port
        self.game = game
        threading.Thread(daemon=True, target=self.__listen).start()
        threading.Thread(daemon=True, target=self.__sendWorker).start()
        
    def terminate(self):
        self.socket.close()


    def __listen(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # fix for address already in use
        logging.info("Binding to port " + str(self.port))
        self.socket.bind((self.ip, self.port))
        self.socket.listen()
        logging.info("From now on listening to connections")
        while True:
            try:
                (client, addr) = self.socket.accept()
            except InterruptedError:
                logging.info("aborted accepting")
                return
            logging.info("new client: "+str(self.currentId))
            with self.lock:
                newID = self.currentId
                self.currentId += 1
                self.clients[newID] = client
            threading.Thread(daemon= True, target=(self.__listenClient), args=(client, newID)).start()

    def __listenClient(self, client, id):
        while True:
            data = ''
            while True:
                try:
                    data += client.recv(2048).decode("utf-8")
                except UnicodeDecodeError:
                    continue
                except BaseException:
                    # disconnected Event
                    Event({"event":"disconnected","sender":id}).execute(self.game)
                    with self.lock:
                        self.clients.pop(id)
                    return
                if "\n" in data:
                    break
            logging.debug("New data: " + data)

            # Fire event
            for dataLine in data.split("\n"):
                if dataLine is not None and dataLine != '':
                    event = self.__eventFromJSON(dataLine, id)
                    if event is not None and event.event is not None:
                        if not event.event is "disconnected":
                            event.execute(self.game)

    def __send(self, id :int, data :str):
        if self.clients is None:
            logging.critical("Error: first call start()")
            return
        try:
            #logging.debug("sending: "+data)
            self.clients[id].send((data + "\n").encode("utf-8"))
        except BaseException:
            logging.info("Tried to send to closed socket -> remove player")

            # disconnected Event
            Event({"event": "disconnected", "sender": id}).execute(self.game)
            with self.lock:
                self.clients.pop(id)
            return

    def send(self, id :int, event :Event):
        self.sendQueue.put((id, self.__eventToJSON(event)))

    def broadcast(self, event :Event):
        self.sendQueue.put(self.__eventToJSON(event))
        
    
    
    def __sendWorker(self):
        while True:
            element = self.sendQueue.get()
            if type(element) is tuple:
                (client, data) = element
                self.__send(client, data)
            elif type(element) is str:
                with self.lock:
                    clientCopies = self.clients.copy()
                for client in clientCopies:
                    self.__send(client, element)
    

    def __eventFromJSON(self, jsonstr, senderId):
        try:
            decode = json.JSONDecoder().decode(jsonstr)
            if type(decode) != dict:
                logging.error(str(senderId) + " sent no valid json: " + jsonstr)
                return None
        except json.JSONDecodeError:
            logging.error(str(senderId) + " sent malformed json: " + jsonstr)
            return None
        return Event(decode, senderId)

    def __eventToJSON(self, event :Event):
        class ComplexEncoder(json.JSONEncoder):
            def default(self, obj):
                if hasattr(obj, 'reprJSON'):
                    return obj.reprJSON()
                else:
                    return json.JSONEncoder.default(self, obj)
        
        decode = json.dumps(event.reprJSON(), cls=ComplexEncoder)
        return decode
        

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format="%(levelname)s %(message)s", stream=sys.stdout)
    server = ServerThread()
    game = Game(server)
    server.start(game)

    # Test
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    logging.info("connecting to localhost:"+str(PORT))
    s.connect((IP, PORT))
    logging.info("Sending to socket...")
    s.send('{"event":"test", "message":"testing is cool!"}\n'.encode("utf-8"))
    time.sleep(1)
    logging.info("Network test successful")
    
