#!/usr/bin/env python3.6
# coding=utf-8
import logging
import signal
import threading
import sys
import getopt

import time

import select

from client import HumanClient, simpleClient
from game import Game
from network import ServerThread
from settings import *




if __name__ == '__main__':
    argv = sys.argv[1:]
    logging.basicConfig(level=logging.INFO, datefmt="%H:%M:%S", format="%(asctime)s-%(levelname)s %(message)s", stream=sys.stdout)

    port = PORT
    ip = IP
    startGui = True
    humanAmount = 2
    dummyAmount = 2
    helpMessage = "main.py -h (help) -i <ip> -p <port> -g (to start no gui) -c <humanClientAmount <= 2> -d <dummyClientAmount>"
    try:
        opts, args = getopt.getopt(argv, "hi:p:gc:d:")
    except getopt.GetoptError:
        logging.critical(helpMessage)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            logging.info(helpMessage)
            sys.exit()
        if opt == '-i':
            ip = arg
        if opt == '-p':
            port = int(arg)
        if opt == '-g':
            startGui = False
            if humanAmount != 0:
                humanAmount = 0
                logging.debug("human player amount set to 0")
        if opt == '-c':
            if not startGui:
                if humanAmount != 0:
                    logging.debug("human player amount set to 0")
                humanAmount = 0
            else:
                humanAmount = int(arg)
        if opt == '-d':
            dummyAmount = int(arg)
        
    
    if not startGui and humanAmount >= 1:
        logging.critical("no gui but humanAmount\naborting...")
        sys.exit(2)
    
    
    server = ServerThread()
    game = Game(server)
    humanList = []

    if startGui:
        from gui import Gui
        gui = Gui(game, humanList)
        
    server.start(game, ip, port)
    time.sleep(0.5)
    
    
    if humanAmount >= 1:
        humanList.append(HumanClient("Up", "Down", "Right", "Left", "Return"))
        humanList[0].start()
    if humanAmount >= 2:
        humanList.append(HumanClient("v", "i", "a", "u", "p"))
        humanList[1].start()
    
    def startSimpleClient():
        simpleClient(ip, port)
    for i in range(0,dummyAmount):
        threading.Thread(daemon=True, target=startSimpleClient).start()


    # terminates server in case of abort
    def signal_handler(signal, frame):
        server.terminate()
        time.sleep(0.5)
        logging.info("server terminated")
        sys.exit(0)
    signal.signal(signal.SIGINT, signal_handler)
    
    
    if startGui:
        logging.info("starting GUI")
        gui.start()
    else:
        logging.info("press Ctrl+C to terminate the server")
        while True:
            time.sleep(STEP_TIMING/1000)
            game.step()
    
    server.terminate()
    time.sleep(0.5)
    logging.info("server terminated")