# footballLiebenzell
... a game like [html5.haxball.com](https://html5.haxball.com)


## Overview
You will have to learn three basic functionalities in your programming language:
- TCP network communication
- JSON format
- GUI graphical user interface

From your client, messages should be sent to this server via TCP (find a library in your language).

Messages have the JSON format. There exist JSON libraries for almost all programming languages. You do not have to parse it yourself.

It would be nice to have a GUI to play the game on. Nevertheless, you could skip this step and program an artificial intelligence. But that might be hard.


## ClientToServer Messages
- connect(name)
- connectObserver()
- accel(ax∈[-1,1], ay∈[-1,1])  //stripped to have max length 1
- kick()


## ServerToClient Messages
- connected (id, width, height, playerSize, ballSize, goalSize, goalDepth, team∈{0,1})
- playerJoined (id, name, team∈{0,1})
- playerLeft (id)
- gameState (players:[id,x,y,sx,sy], ball:(x,y,sx,sy))
- goal(team)


## Example Messages:
### Accelerate:
{
	"event":"accel",
	"ax":1.0,
	"ay":0.0
}

### Game State:
{
	"event": "gameState",
	"players":[
		{"x": 200.0, "y": 167.0, "sx": 0.0, "sy": 0.0, "id": 0}, 
		{"x": 600.0, "y": 167.0, "sx": 0.0, "sy": 0.0, "id": 1}
	],
	"ball": {"x": 400.0, "y": 200.0, "sx": 0.0, "sy": 0.0}
}


## TODO
- document messages more
- test serverToClient events
- maybe improve physics?
- draw nicer Field
- team colors
- run-feature(every 5s you can run for 1s)?

## Usage
- install python3.6 with tkinter
- run python main.py

![Screenshot](https://gitlab.com/LuckyLukert/footballLiebenzell/raw/master/screenshot.png)
